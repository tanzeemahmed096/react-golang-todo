package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/tanzeem/golang-react-todo/routes"
)

func main() {
	r := routes.Router()
	fmt.Println("Starting the server on the port 9000...")

	log.Fatal(http.ListenAndServe(":9000", r))
}
