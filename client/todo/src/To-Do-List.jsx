import axios from "axios"
import { useState } from "react"
import {Card, Header, Form, Input, Icon} from "semantic-ui-react"

let endpoint = "http://localhost:9000"

const ToDo = (props) => {
    const items = useState([])
    const task = useState("")

    const handleSubmit = () => {
        
    }

    return (
        <div>
            <div className="row">
            <Header className="header" as="h2" color="yellow"/>
            </div>
            <div className="row">
                <Form onSubmit={handleSubmit}>
                <Input type="text" name="task" value={task} onChange={handleChange} fluid placeholder="create task"/>
                </Form>
            </div>
            <div className="row">
                <Card.Group>{items}</Card.Group>
            </div>
        </div>
    )
}

export default ToDo